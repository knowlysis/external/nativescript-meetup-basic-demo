import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import * as camera from 'nativescript-camera';
import { ImageSource, fromAsset, fromResource, fromFile } from 'tns-core-modules/image-source';

@Component({
    selector: "Featured",
    moduleId: module.id,
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    imageSrc: ImageSource;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.imageSrc = <ImageSource> fromFile('~/assets/images/default.jpg');
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    async takePicture() {
        const imageAsset = await camera.takePicture();
        this.imageSrc = await <Promise<ImageSource>> fromAsset(imageAsset)
    }
}
