import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { alert } from "tns-core-modules/ui/dialogs"
import { EventData } from "tns-core-modules/ui/page/page";
import { Slider } from "tns-core-modules/ui/slider/slider";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { Switch } from "tns-core-modules/ui/switch/switch";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    sliderValue: number;
    progressValue: number;
    pokemon: Array<any>;
    selectedPokemon: string;
    interval;
    progressActive: boolean;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.sliderValue = 10;
        this.progressValue = 5;
        this.pokemon = [
            'Bulbasaur',
            'Charmander',
            'Squirtle',
            'Pikachu',
            'Ditto',
            'Mr. Mime'
        ];
        this.selectedPokemon = 'Mewtwo';
        this.progressActive = false;

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onSliderValueChange(args: EventData): void {
        const slider = <Slider>args.object;
        this.sliderValue = slider.value;
    }

    onSelectedIndexChanged(args: EventData): void {
        const picker = args.object as ListPicker;
        this.selectedPokemon = this.pokemon[picker.selectedIndex];
    }

    onTap(args: EventData): void {
        
        const options = {
            title: 'Pokemon Selection',
            message: `You chose: ${this.selectedPokemon}`,
            okButtonText: 'OK'
        };
        
        alert(options).then(() => {
            console.log('done!');
        });
    }

    startProgress() {
        this.progressActive = true;
        this.interval = setInterval(() => {
            this.progressValue += 1;
        }, 300);
    }

    stopProgress() {
        this.progressActive = false;
        clearInterval(this.interval);
    }

    onSwitchChange(args: EventData) {
        const mySwitch = args.object as Switch;
        if (mySwitch.checked) {
            this.startProgress();
        } else {
            this.stopProgress();
        }
    }

}