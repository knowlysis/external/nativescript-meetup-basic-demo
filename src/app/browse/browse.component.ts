import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import { WebView, LoadEventData } from "tns-core-modules/ui/web-view";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";


@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {

    @ViewChild('myWebView', { read: ElementRef, static: true}) webViewRef: ElementRef;
    @ViewChild('myActivityIndicator', { read: ElementRef, static: true}) activityRef: ElementRef;

    showWebView: boolean;
    webviewUrl: string;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.webviewUrl = 'https://docs.nativescript.org/angular/';
        this.showWebView = false;
    }

    ngAfterViewInit(): void {
        const webView: WebView = this.webViewRef.nativeElement;
        const activityIndicator: ActivityIndicator = this.activityRef.nativeElement;
        
        activityIndicator.busy = true;
        webView.visibility = 'hidden';

        webView.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
            activityIndicator.busy = false;
            webView.visibility = 'visible';
            console.log('here');
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
